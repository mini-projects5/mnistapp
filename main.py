import numpy as np
from ToNNi import *
import tensorflow as tf
import cv2
import os
import PySimpleGUI as sg
import os.path
import PIL.Image
import io
import base64


def resize_image(image_path, resize=None): #image_path: "C:User/Image/img.jpg"
    if isinstance(image_path, str):
        img = PIL.Image.open(image_path)
    else:
        try:
            img = PIL.Image.open(io.BytesIO(base64.b64decode(image_path)))
        except Exception as e:
            data_bytes_io = io.BytesIO(image_path)
            img = PIL.Image.open(data_bytes_io)

    cur_width, cur_height = img.size
    if resize:
        new_width, new_height = resize
        scale = min(new_height/cur_height, new_width/cur_width)
        img = img.resize((int(cur_width*scale), int(cur_height*scale)), PIL.Image.ANTIALIAS)
    bio = io.BytesIO()
    img.save(bio, format="PNG")
    del img

    return bio.getvalue()


(X_train, Y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()

assert X_train.shape == (60000, 28, 28)
assert x_test.shape == (10000, 28, 28)
assert Y_train.shape == (60000,)
assert y_test.shape == (10000,)

X_train = np.reshape(X_train,(-1,784))
X_train = X_train/255

model = Model(0.001 ,70,output_size=10,batch_size=128)
Wb = model.fit(X_train.T, Y_train)
dataHandler.writeData(Wb)

layout = [[sg.Text("HII")],[sg.Button("OK")]]
file_list_column = [
    [
        sg.Text("Image Folder"),
        sg.In(size=(25,1), enable_events=True, key="-FOLDER-"),
        sg.FolderBrowse()
    ],
    [
        sg.Listbox(
            values = [],
            enable_events=True,
            size = (40,20),
            key="-FILE LIST-"
        )
    ]
]

image_viewer_column = [
    [sg.Text("Chose image:")],
    [sg.Text(size=(80,1),key="-TOUT-")],
    [sg.Image(key="-IMAGE-")]
]

predict_column =[
    [sg.Button("Get Prediction: ",key="baton")],
    [sg.Text("PREDICTION: ",font=("Arial", 40))],
    [sg.Text(size=(20,1),key="-TEXT-")]]

layout = [[
    sg.Column(file_list_column),
    sg.VSeparator(),
    sg.Column(image_viewer_column),
    sg.VSeparator(),
    sg.Column(predict_column)
]]
window = sg.Window("demo", layout,size=(1600,800))

while True:
    event,values = window.read()

    if event == "OK" or event == sg.WIN_CLOSED:
        break
    
    if event == "-FOLDER-":
        folder = values["-FOLDER-"]
        try:
            file_list = os.listdir(folder)
        except:
            file_list = []

        fnames = [f
            for f in file_list
                if os.path.isfile(os.path.join(folder,f))
                and f.lower().endswith((".png",".gif"))
        ]
        window["-FILE LIST-"].update(fnames)
    elif event == "-FILE LIST-":
        try:

            filename = os.path.join(values["-FOLDER-"], values["-FILE LIST-"][0])
            #print(filename)

            
            window["-TOUT-"].update(filename)
            window["-IMAGE-"].update(data=resize_image(filename,resize=(400,400)))          
        except:
            pass

    if event == "baton":
        im = cv2.imread(filename,cv2.IMREAD_GRAYSCALE)
        im = cv2.resize(im,(28,28))
        c=0
        for i in range(28):
            c += im[1,i]
        if c>3500:
            im = cv2.bitwise_not(im)
            pass
        #cv2.imshow("im",im)
        #cv2.waitKey()
        #cv2.destroyAllWindows()
        im = im.flatten()
        im = np.expand_dims(im,axis=0)
        window["-TEXT-"].update(str(model.predict(im.T)[0]))
        window["-TEXT-"].update(font=("Arial", 80))


window.close()
